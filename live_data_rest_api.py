from flask import Flask
from flask_restplus import Resource, Api
import pandas_datareader as pdr
import logging
import datetime

LOG = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app)


@api.route('/<string:ticker>/<string:ticker2>')
class Price(Resource):
    def get(self, ticker, ticker2):
        start_date = datetime.datetime.now() - datetime.timedelta(6)

        df = pdr.get_data_yahoo([ticker, ticker2], start_date)[
            ['Close', 'High', 'Open']]

        print(df.to_json())

        df['Date'] = df.index
        return df.to_json()


if __name__ == '__main__':
    app.run(debug=True)
