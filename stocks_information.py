import pandas as pd
import pandas_datareader as pdr


# import datetime internal datetime module
# datetime is a Python module
import datetime

# datetime.datetime is a data type within the datetime module
start = datetime.datetime(2019, 11, 1)
end = datetime.datetime(2019, 11, 2)

# DataReader method name is case sensitive
df = pdr.DataReader('nvda', 'yahoo', start, end)

# invoke to_csv for df dataframe object from
# DataReader method in the pandas_datareader library


# ..\first_yahoo_prices_to_csv_demo.csv must not
# be open in another app, such as Excel
path_out = 'C:\\Users\\Administrator\\Documents\\pythonhackathon'

df.to_csv(path_out + 'exported_data.csv')
