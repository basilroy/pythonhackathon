import requests
import json
import logging
from urllib3 import HTTPConnectionPool
SLACK_URL = "https://hooks.slack.com/services/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"

SITES = "http://www.google.c0m"


def req_to_slack(text):
    return requests.post(SLACK_URL, json.dumps({'text': text}))


def check_availabilty():
    try:
        page = requests.get(SITES)
        return (page.status_code)
    except:
        return 0


def main():

    res = check_availabilty()
    if res != 200:
        req_to_slack("Sites down - DG3")


if __name__ == "__main__":
    main()
   # print('heloo')
